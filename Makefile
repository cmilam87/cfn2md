clean:
	rm -rf build/
	rm -rf dist/
	rm -rf cfn2md.egg-info/

test:
	python setup.py test
	flake8 cfn2md
	pylint cfn2md/

build: clean
	python setup.py sdist

build_whl: clean
	python setup.py bdist_wheel --universal

travis: test clean build
