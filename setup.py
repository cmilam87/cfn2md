"""Packaging settings."""

from codecs import open as codecs_open
from os.path import abspath, dirname, join
from sys import version_info

from setuptools import find_packages, setup

from cfn2md import __version__


THIS_DIR = abspath(dirname(__file__))
with codecs_open(join(THIS_DIR, 'README.md'), encoding='utf-8') as readfile:
    LONG_DESCRIPTION = readfile.read()


INSTALL_REQUIRES = [
    'docopt',
    'future',
    'cfn-flip'
]

setup(
    name='cfn2md',
    version=__version__,
    description='Create markdown readme using YAML/JSON CloudFormation templates',
    long_description=LONG_DESCRIPTION,
    url='https://github.com/onicagroup/SOMELINK',
    author='Onica Group LLC',
    author_email='opensource@onica.com',
    license='Apache License 2.0',
    classifiers=[
        'Intended Audience :: Developers',
        'Topic :: Utilities',
        'Natural Language :: English',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
    ],
    python_requires='>=2.6',
    keywords='cli',
    packages=find_packages(exclude=['docs', 'tests*']),
    install_requires=INSTALL_REQUIRES,
    extras_require={
        'test': ['flake8', 'pep8-naming', 'flake8-docstrings', 'pylint'],
    },
    entry_points={
        'console_scripts': [
            'cfn2md=cfn2md.cli:main',
        ],
    }
)
