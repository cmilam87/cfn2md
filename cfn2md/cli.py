"""
cfn2md Overview.

Usage:
  cfn2md <file> [<markdown>] [--title=<title>]

Options:
  file                              Input file to transform
  [markdown]                        The output markdown file
  [--title=<title>]                 The title to put as the header
  -h --help                         Show this screen.
  --version                         Show version.

Help:
  * If the markdown file is not specified it will be
    placed in the current directory as README.md
  * If no title is specified then the description
    section will be used as the title
"""
from __future__ import print_function
import os
import sys
import logging
import yaml
from docopt import docopt
from cfn_flip import load
from . import __version__ as version


def convert(raw_json, markdown, title): # noqa pylint: disable=too-many-branches
    """
    Convert JSON to Markdown.

    This function parses JSON file, stores the information into
    variables, and then manipulates the variables to create a beautiful
    markdown file.
    """
    data = []
    markdown = markdown or 'README.md'

    if 'AWSTemplateFormatVersion' in raw_json: # noqa pylint: disable=too-many-nested-blocks
        if raw_json['AWSTemplateFormatVersion'] == '2010-09-09':
            if 'Description' in raw_json:
                description = raw_json['Description']
                if title:
                    data.append('# %s\n%s' % (title, description))
                else:
                    data.append('## Description\n\n %s\n' % description)

            if 'Conditions' in raw_json:
                data.append('## {}'.format('Conditions'))
                for name, data in raw_json['Conditions'].iteritems():
                    data.append(name)

            if 'Parameters' in raw_json:
                data.append('## Parameters')
                for name, info in raw_json['Parameters'].iteritems():
                    param = '* %s' % name
                    if 'Type' in info:
                        param += ' (`%s`)' % info['Type']
                    if 'Description' in info:
                        param += '\n\t- %s' % info['Description']

                    data.append(param)

                data.append('\n')

            if 'Resources' in raw_json:
                data.append('## {}'.format('Resources'))
                for name, info in raw_json['Resources'].iteritems():
                    resource = '* %s' % name
                    if 'Type' in info:
                        resource += ' (`%s`)' % info['Type']

                    data.append(resource)
                data.append('\n')

            if 'Mappings' in raw_json:
                data.append('## Mappings')
                for name, info in raw_json['Mappings'].iteritems():
                    mappings = '* %s' % name
                    for mapping in info.iteritems():
                        mappings += '\n\t- %s' % mapping[0]
                        for maps in mapping[1].iteritems():
                            mappings += "\n\t\t- %s = %s" % (maps[0], maps[1])
                    data.append(mappings)
                data.append('\n')

            if 'Outputs' in raw_json:
                data.append('## {}'.format('Outputs'))
                for name, info in raw_json['Outputs'].iteritems():
                    output = '* %s' % name
                    if 'Description' in info:
                        output += '\n\t- %s' % info['Description']

                    data.append(output)
                data.append('\n')

            with open(markdown, 'w') as stream:
                for row in data:
                    stream.write('%s\n' % str(row))
    else:
        print('This is not a CFN Template')


def main():
    """Provide main CLI entrypoint."""
    if os.environ.get('DEBUG'):
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)

    options = docopt(__doc__, version=version)

    input_file = options['<file>']
    markdown = options['<markdown>'] or 'README.md'
    title = options['--title']

    if not os.path.isfile(input_file):
        logging.error('File not found: %s', input_file)
        sys.exit(1)

    try:
        with open(input_file) as stream:
            data, in_format = load(stream.read())
    except yaml.scanner.ScannerError:
        logging.error('Could not determine the input format')
        sys.exit(1)

    logging.info("%s format detected", in_format)
    convert(data, markdown, title)
