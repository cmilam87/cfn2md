# CFN2MD

## What is CFN2MD?
CFN2MD allows users to convert YAML or JSON CloudFormation templates into beautiful markdown.
It outputs each section with its own headers, description, and properties (if applicable)

## Installation

To install locally
```
cd cfn2md
pip install --user .
```
